﻿namespace Bots

open System
open System.IO
open System.Net.Sockets
open FSharp.Data

[<AbstractClass>]
type Bot(name : string, filename : string option) =

    // MUTABLE STATE: OMG DON'T TELL ANYONE
    let mutable track : JsonGameInit.Message = null
    let mutable previousTicks = 0
    let mutable previousPieceLength = 0m
    let mutable previousPieceIndex = 0
    let mutable previousInPieceDistance = 0m
    let mutable throttle = 1.
    let mutable speed = 0.
    let mutable targetSpeed = 5.0
    let mutable acceleration = 0.
    let mutable targetAcceleration = 0.
    let mutable useTurbo = false
    let mutable switchLeft = false
    let mutable switchRight = false
//    let mutable previousSlip = 0.
//    let mutable previousCurve = 0.
//    let mutable calculatedSidewaysForce = 0.
//    let mutable previousCalculatedSlip = 0.

    member this.Name = name
    member this.IsMyBot(other : string) = this.Name = other
    member this.Log(msg : string) =
        Console.WriteLine(sprintf "%s: %s" name msg)
    member this.Throttle with get() = throttle and set value = throttle <- value
    member this.UseTurbo with get() = useTurbo and set value = useTurbo <- value
    member this.SwitchLeft with get() = switchLeft and set value = switchLeft <- value
    member this.SwitchRight with get() = switchRight and set value = switchRight <- value    
    member this.Speed with get() = speed and private set value = speed <- value
    member this.TargetSpeed with get() = targetSpeed and set value = targetSpeed <- value
    member this.Acceleration with get() = acceleration and private set value = acceleration <- value
    member this.TargetAcceleration with get() = targetAcceleration and set value = targetAcceleration <- value

    member this.ProcessMessage (msg : Json.Message) : Move option =
        match msg.MsgType with                
        | "join"           -> this.Join msg; Some Ping
        | "joinRace"       -> this.JoinRace msg; Some Ping
        | "gameInit"       ->
            track <- JsonGameInit.Parse (msg.JsonValue.ToString())
            this.GameInit track
            Some Ping
        | "gameStart"      ->
            match filename with
            | None -> ()
            | Some x -> 
                if File.Exists(x) then
                    File.Delete(x)
                File.AppendAllText(x, "Tick, Lap, Piece, Throttle*5, Acceleration*10, TargetAcceleration*10, TargetSpeed, Speed, Angle/10, (1/Radius)*1000, Slip/10\r\n");
            this.GameStart msg
            Some (Throttle this.Throttle)
        | "gameEnd"        -> this.GameEnd msg; None
        | "carPositions"   ->
            let msg = JsonCarPositions.Parse(msg.JsonValue.ToString())
            this.CarPositions msg
            this.Update (track, msg)
            this.LogStats msg
            if useTurbo then
                useTurbo <- false
                this.Log "Using boost! OMG!"
                Some Turbo
            else if switchLeft then
                switchLeft <- false
                this.Log "Switching left!"
                Some SwitchLeft
            else if switchRight then
                switchRight <- false
                this.Log "Switching right!"
                Some SwitchRight
            else
                Some(Throttle this.Throttle)
        | "turboAvailable" -> this.TurboAvailable msg; None
        | "crash"          -> this.Crash msg; None
        | "lapFinished"    -> this.LapFinished msg; None
        | _ ->
            msg.JsonValue |> printfn "Unknown message: %A"
            None

    member private this.LogStats msg =
        let myCar = msg.Data |> Array.find (fun car -> car.Id.Name = this.Name)

        let distanceTravelled =
            if myCar.PiecePosition.PieceIndex = previousPieceIndex then
                myCar.PiecePosition.InPieceDistance - previousInPieceDistance
            else
                let a = myCar.PiecePosition.PieceIndex
                myCar.PiecePosition.InPieceDistance + (max 0m (previousPieceLength - previousInPieceDistance))

        // HACK: GameTick is missing on first one; causes crash
        let gameTick = 
            try
                msg.GameTick
            with
                | _ -> 0
            
        let newSpeed =
            if gameTick - previousTicks <= 0 then
                0.
            else
                (float distanceTravelled) / float (gameTick - previousTicks)
        this.Acceleration <- newSpeed - this.Speed
        this.Speed <- newSpeed

        let currentPiece = track.Data.Race.Track.Pieces.[myCar.PiecePosition.PieceIndex]
        previousPieceLength <- Track.GetLength track currentPiece myCar.PiecePosition.Lane.StartLaneIndex |> decimal
        previousPieceIndex <- myCar.PiecePosition.PieceIndex
        previousInPieceDistance <- myCar.PiecePosition.InPieceDistance
        previousTicks <- gameTick


//            // Slip = (previousSlip + (f(speed, 1/angle))) * decay
//            let slipDecay = 0.5
//            let slipAccelerationDecay = 0.5
//            let speedFactor = 1.
//            let speedPower = 1.5
//            let curve = (1./(defaultArg (Option.map float currentPiece.Radius) Double.PositiveInfinity))
//            let sidewaysForce = ((this.Speed ** speedPower) * speedFactor * curve)
//            calculatedSidewaysForce <- (calculatedSidewaysForce * slipAccelerationDecay) + sidewaysForce
//            let expectedSlip =
//                (previousCalculatedSlip * slipDecay) + calculatedSidewaysForce
//
//            
//
//            // Store previous slip for next frame
//            previousCurve <- curve
//            previousSlip <- float myCar.Angle
//            previousCalculatedSlip <- expectedSlip
//
        if filename.IsSome then
             File.AppendAllText(filename.Value, (sprintf "%O, %O, %O, %O, %O, %A, %O, %O, %O, %O, %O\r\n" gameTick myCar.PiecePosition.Lap myCar.PiecePosition.PieceIndex (this.Throttle * 5.) (this.Acceleration * 10.) (this.TargetAcceleration * 10.) targetSpeed this.Speed ((defaultArg currentPiece.Angle 0m)/10m) (1./(defaultArg (Option.map float currentPiece.Radius) Double.PositiveInfinity)*1000.) ((float myCar.Angle)/10.)))
//                File.AppendAllText(
//                    filename.Value,
//                    (sprintf "%O, %O, %O, %O, %A, %O, %O, %O, %O, %O\r\n"
//                    gameTick
//                    myCar.PiecePosition.Lap
//                    myCar.PiecePosition.PieceIndex
//                    (this.Throttle * 5.)
//                    (this.Acceleration * 10.)
//                    this.Speed
//                    (curve * 100.)
//                    ((float (defaultArg currentPiece.Radius 0))/100.)
//                    ((float myCar.Angle)/10.)
//                    (expectedSlip/10.))
//                )

    // These members and their default implementations are for subclasses only; don't
    // add logic to them, as then subclasses must remember to call base. Instead; handle
    // logic in the ProcessMessage method

    abstract member Join:           Json.Message -> unit
    abstract member JoinRace:       Json.Message -> unit
    abstract member GameInit:       JsonGameInit.Message -> unit
    abstract member GameStart:      Json.Message -> unit
    abstract member GameEnd:        Json.Message -> unit
    abstract member TurboAvailable: Json.Message -> unit
    abstract member LapFinished:    Json.Message -> unit
    abstract member Crash:          Json.Message -> unit
    abstract member CarPositions:   JsonCarPositions.Message -> unit
    abstract member Update:         JsonGameInit.Message * JsonCarPositions.Message -> unit

    default this.Join msg = ()
    default this.JoinRace msg = ()
    default this.GameInit msg = ()
    default this.GameStart msg = ()
    default this.GameEnd msg = ()
    default this.TurboAvailable msg = ()
    default this.LapFinished msg = ()
    default this.Crash msg = ()
    default this.CarPositions msg = ()
    default this.Update (track, cars) = ()
