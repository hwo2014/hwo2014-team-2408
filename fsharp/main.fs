module m

open System
open System.IO
open System.Net.Sockets
open System.Threading
open System.Threading.Tasks
open FSharp.Data
open Bots

type RaceType =
    | Solo
    | Comparison

let run host port botKey track colour raceType (bot : Bot) =
    printfn "Connecting to %s:%d as %s/%s" host port bot.Name botKey

    use client = new TcpClient(host, port)
    let stream = client.GetStream()
    let reader = new StreamReader(stream)
    let writer = new StreamWriter(stream)

    match raceType with
    | Solo -> send (join bot.Name botKey track colour) writer
    | Comparison -> send (joinRace bot.Name botKey track colour) writer

    let processNextLine nextLine =
        match nextLine with
        | null -> false
        | line ->
            let msg = Json.Parse(line)
            let move = bot.ProcessMessage msg
            match move with
            | None -> ()
            | Some Ping ->
                //printfn "Sending ping for %A" msg.GameTick
                send (ping msg.GameTick) writer
            | Some (Throttle x) ->
                //printfn "Sending throttle %f for %A" x msg.GameTick
                send (throttle x msg.GameTick) writer
            | Some Turbo ->
                //printfn "Sending turbo for %A" msg.GameTick
                send (turbo) writer
            | Some SwitchLeft ->
                send (switchLeft) writer
            | Some SwitchRight ->
                send (switchRight) writer
            true

    while processNextLine (reader.ReadLine()) do 
        ()


[<EntryPoint>]
let main args =
    let (host, portString, botName, botKey) = 
      args |> (function [|a;b;c;d|] -> a, b, c, d | _ -> failwith "Invalid param array")
    let port = Convert.ToInt32(portString)

    //let runComparison = false
    let runComparison = Environment.UserInteractive

    if runComparison then
        let runBot = run "prost.helloworldopen.com" port botKey

        let car1 = Task.Run (fun () ->                    runBot "keimola" "red" Comparison <| BoostingBot(botName + " Keimola", Some("..\Data.Keimola.csv"), 0.75))
        let car2 = Task.Run (fun () ->                    runBot "germany" "red" Comparison <| BoostingBot(botName + " Germany", Some("..\Data.Germany.csv"), 0.75))
        let car3 = Task.Run (fun () ->                    runBot "usa"     "red" Comparison <| BoostingBot(botName + " USA", Some("..\Data.USA.csv"), 0.75))
        let car4 = Task.Run (fun () ->                    runBot "france"  "red" Comparison <| BoostingBot(botName + " France", Some("..\Data.France.csv"), 0.75))

        Task.WaitAll(car1, car2, car3, car4);
    else if Environment.UserInteractive then
        let runBot = run "prost.helloworldopen.com" port botKey
        //runBot "usa" "red" Comparison <| LaneBot(botName, Some("..\Data.csv"), Inside)
        runBot "usa" "red" Comparison <| BoostingBot(botName, Some("..\Data.csv"), 1.)//0.78)
    else
        run host port botKey "keimola" "red" Solo <| BoostingBot(botName, None, 0.75)
    
    0