﻿module Track

open System

let GetLength (track : JsonGameInit.Message) (piece : JsonGameInit.Piecis) lane =
    match piece.Length with
    | Some x -> float x
    | None ->
        let radius = float piece.Radius.Value
        let distanceFromCenter = float track.Data.Race.Track.Lanes.[lane].DistanceFromCenter
        let angle = float piece.Angle.Value
        let trackRadius =
            if angle > 0. then
                radius - distanceFromCenter
            else
                radius + distanceFromCenter
        trackRadius * ((angle |> Math.Abs) * (Math.PI / 180.))

let Clamp min max x =
    if x < min then
        min
    else if x > max then
        max
    else
        x
let ClampThrottle = Clamp 0. 1.
