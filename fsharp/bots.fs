﻿namespace Bots

open System

// Bot drives at constant speed
type ConstantBot(name : string, filename : string option) =
    inherit Bot(name, filename)
    do
        base.Throttle <- 0.6

// Bot tweaks acceleration to maintain a target speed
type TargetSpeedBot(name : string, filename : string option) =
    inherit Bot(name, filename)

    do
        base.Throttle <- 1.

    let maxAcceleration = 0.5
    let fullAccelerationThreshold = 0.65 // If we're more than this away from target speed, we do full accel/decell
    let epsilon = Double.Epsilon // Used for float comparisons
    let throttleAdjustmentPerTick = 3.
    
    override this.Update(track, cars) =
        // Target is from -1 to 1 (where 0 is maintain speed, -1 is full decel, 1 is full accel)
        this.TargetAcceleration <-
            if this.Speed > (this.TargetSpeed - epsilon) && this.Speed < (this.TargetSpeed + epsilon) then
                0. // Don't change
            else
                let distanceFromTargetSpeed = this.TargetSpeed - this.Speed
                let targetAccelerationScale = Track.Clamp -1. 1. (distanceFromTargetSpeed / fullAccelerationThreshold)
                targetAccelerationScale * maxAcceleration

        let throttleAdjustment =
            if (this.Acceleration > this.TargetAcceleration - epsilon) && (this.Acceleration < this.TargetAcceleration + epsilon) then
                0.
            else
                throttleAdjustmentPerTick * (this.TargetAcceleration - this.Acceleration)

        this.Throttle <- Track.ClampThrottle (this.Throttle + throttleAdjustment)


// Bot adjusts target speed in advance of upcoming pieces
type AnticipatingBot(name : string, filename : string option) =
    inherit TargetSpeedBot(name, filename)
        
    let mutable maxSpeeds = [||]
    member this.MaxSpeeds with get() = maxSpeeds and set value = maxSpeeds <- value

    override this.Update (track, cars) =
        let myCar = cars.Data |> Array.find (fun car -> car.Id.Name = this.Name)
        let targetPieceIndex = this.GetTargetPieceIndex track myCar.PiecePosition
        base.TargetSpeed <- this.GetTargetSpeed targetPieceIndex
        base.Update(track, cars)

    // Get "target" piece; this is the piece we will use the speed from
    // This will be the "current" (cars) piece mostly; but when the "next piece
    // requires a much slower speed, we will switch to it earlier
    member this.GetTargetPieceIndex track piece =
        let maxExpectedChange = 10.
        let distanceForMaxExpectedChange = 320.

        let straightPieces =
            track.Data.Race.Track.Pieces |> Seq.append track.Data.Race.Track.Pieces // Double up the track so we can loop around
            |> Seq.skip piece.PieceIndex // Skip to current piece
            |> Seq.takeWhile (fun p -> p.Radius.IsNone) // Take all pieces that are straight
            |> Seq.toArray

        let distanceTillNextBend =
            (
                if Array.isEmpty straightPieces then
                    Track.GetLength track track.Data.Race.Track.Pieces.[piece.PieceIndex] piece.Lane.StartLaneIndex
                else
                    straightPieces
                    |> Seq.map (fun p -> p.Length.Value)
                    |> Seq.sum
                    |> float
            ) - (float piece.InPieceDistance)

        let indexOfNextChange = (piece.PieceIndex + (max straightPieces.Length 1)) % track.Data.Race.Track.Pieces.Length
        let speedForNextChange =
            maxSpeeds.[indexOfNextChange]

        if distanceTillNextBend > distanceForMaxExpectedChange then
            piece.PieceIndex
        // Else, if the next piece is faster, also target the current piece
        else if speedForNextChange > this.Speed then
            piece.PieceIndex
        else
            indexOfNextChange

    member this.GetTargetSpeed pieceIndex =
        maxSpeeds.[pieceIndex]


type TestBotState =
    | CalculatingSafeSpeed
    | CalculatingMaxSlippage
    | Driving

type TestBot(name : string, filename : string option, baseThrottle : float) =
    inherit AnticipatingBot(sprintf "%s %f" name baseThrottle, filename)
    
    let mutable minWantedSlippage = 0.
    let mutable maxWantedSlippage = 10000.
    let mutable maxSeenSlippage = 0.

        
    let mutable state = CalculatingSafeSpeed
    let mutable maxDiscoverySpeed = 0.
    let mutable speedIncreaseAmount = 0.
    let mutable maxSpeedOfCurrentPiece = 0.
    let mutable maxSlippageOfCurrentPiece = 0.
    let mutable previousPieceIndex = -1
    let mutable hasFoundMaxSpeeds = [||]

    do
        base.Throttle <- baseThrottle

    override this.Crash msg =
        if msg.Data.Record.Value.Name.Value = this.Name then
            maxWantedSlippage <- maxSeenSlippage * 0.9
            minWantedSlippage <- maxSeenSlippage * 0.6
            printfn "Crashed with max slippage %f" maxSeenSlippage
            state <-
                match state with
                | CalculatingSafeSpeed -> CalculatingSafeSpeed
                | CalculatingMaxSlippage -> Driving
                | Driving -> Driving

    override this.Update(track, cars) =
        let myCar = cars.Data |> Array.find (fun car -> car.Id.Name = this.Name)
        let currentPieceIndex = myCar.PiecePosition.PieceIndex

        maxSeenSlippage <- max maxSeenSlippage (float myCar.Angle)

        match state with 

        | CalculatingSafeSpeed ->
            // Calculate the max speed we drove around at at half throttle (this is assumed safe)
            maxDiscoverySpeed <- max maxDiscoverySpeed this.Speed
            // Once we've done at least two pieces, we should have enough data to get us started
            if myCar.PiecePosition.PieceIndex = 5 then
                printfn "Discovered speed %f" maxDiscoverySpeed
                // Set the max speed for every piece to be what we calculated and move on
                this.MaxSpeeds <- 
                    track.Data.Race.Track.Pieces
                    |> Array.map (
                        fun p ->
                        match p.Length with
                        | Some x -> maxDiscoverySpeed * 3. // Assume we can probably go at least this speed on the straights
                        | None -> maxDiscoverySpeed * 1.
                    )
                hasFoundMaxSpeeds <- Array.create track.Data.Race.Track.Pieces.Length false
                // Calcualte a "safe" amount to increase target speed by
                speedIncreaseAmount <- maxDiscoverySpeed * 0.04
                state <- CalculatingMaxSlippage

        | CalculatingMaxSlippage ->
            base.Throttle <- 1.0
            // If we got around some way without crashing, just move on
            if myCar.PiecePosition.PieceIndex > 7 then // (Laps are 0-based!!)
                // Set some defaults, as we never actually crashed :/
                maxWantedSlippage <- 57. * 0.9
                minWantedSlippage <- 57. * 0.6
                state <- Driving

        | Driving ->
            // If we changed piece, do some calculations for the previous piece
            if previousPieceIndex <> -1 && previousPieceIndex <> currentPieceIndex then
                let piece = track.Data.Race.Track.Pieces.[previousPieceIndex]

                // If we didn't sleep enough; then increase/decrease the target speed for pieces of this radius
                let mutable hasFoundMaxSpeed = false
                let amountToChangeTargetSpeedBy =
                    if not hasFoundMaxSpeeds.[previousPieceIndex] && maxSlippageOfCurrentPiece < minWantedSlippage then
                        //printfn "Increasing! (only slipped by %f, less than %f); radius %A" maxSlippageOfCurrentPiece minWantedSlippage piece.Radius
                        speedIncreaseAmount
                    else if maxSlippageOfCurrentPiece > maxWantedSlippage then
                        //printfn "Decreasing! (slipped by %f, more than %f); radius %A" maxSlippageOfCurrentPiece maxWantedSlippage piece.Radius
                        hasFoundMaxSpeed <- true
                        -speedIncreaseAmount
                    else
                        hasFoundMaxSpeed <- true
                        0.
                
                let matchingPieces =
                    track.Data.Race.Track.Pieces
                    |> Array.mapi (fun i p -> i, p) // Add indexes
                    |> Array.filter (fun (i, p) -> (Option.map abs p.Radius) = (Option.map abs piece.Radius)) // Filter to matching pieces
                    |> Array.map (fun (i, p) -> i) // Extract index
                matchingPieces
                    |> Array.iter (fun i -> this.MaxSpeeds.[i] <- this.MaxSpeeds.[i] + amountToChangeTargetSpeedBy)
                if hasFoundMaxSpeed then
                    matchingPieces
                        |> Array.iter (fun i -> hasFoundMaxSpeeds.[i] <- true)

                // Reset variables for this piece
                maxSpeedOfCurrentPiece <- 0.
                maxSlippageOfCurrentPiece <- 0.

            maxSpeedOfCurrentPiece <- max maxSpeedOfCurrentPiece this.Speed
            maxSlippageOfCurrentPiece <- max maxSlippageOfCurrentPiece (float (abs myCar.Angle))
            
            // Let base class take care of setting target speed
            base.Update(track, cars)

        previousPieceIndex <- currentPieceIndex

type BoostingBot(name : string, filename : string option, baseThrottle : float) =
    inherit TestBot(name, filename, baseThrottle)

    let mutable turboAvailable = false
    let mutable turboTicks = 0

    let mutable longestStraightLength = 0
    let mutable longestStraightStartIndexes = [||]

    override this.TurboAvailable msg =
        turboAvailable <- true
        turboTicks <- msg.Data.Record.Value.TurboDurationTicks.Value

    override this.GameInit track =
        let pieces = track.Data.Race.Track.Pieces
        let piecesWrapped = Seq.append pieces pieces
        let straightLengths =
            seq { 0 .. track.Data.Race.Track.Pieces.Length - 1 }
            |> Seq.map (fun i -> 
                match pieces.[i].Length with
                | Some _ ->
                    piecesWrapped
                    |> Seq.skip i
                    |> Seq.takeWhile (fun p -> p.Length.IsSome)
                    |> Seq.map (fun p -> p.Length.Value)
                    |> Seq.sum
                | None -> 0
            )
        longestStraightLength <- straightLengths |> Seq.max
        
        longestStraightStartIndexes <- straightLengths |> Seq.zip <| Seq.initInfinite id |> Seq.filter (fun (l, i) -> l = longestStraightLength) |> Seq.map (fun (l, i) -> i) |> Seq.toArray
        //longestStraightStartIndexes <- longestStraightStartIndexes |> Seq.take 1 |> Seq.toArray
        this.Log <| sprintf "Longest straights are %A" longestStraightStartIndexes

        base.GameInit track

    override this.Update (track, cars) =
        let myCar = cars.Data |> Array.find (fun car -> car.Id.Name = this.Name)
        let onLongestStraight = Array.exists (fun l -> l = myCar.PiecePosition.PieceIndex) longestStraightStartIndexes
        if turboAvailable && onLongestStraight then
            turboAvailable <- false
            this.UseTurbo <- true

        base.Update (track, cars)

type LaneBot(name : string, filename : string option, lane : LanePreference) =
    inherit BoostingBot(name, filename, 0.78)

    // HACK: Switch right twice
    let mutable switchsRemaining = 2
    let mutable switchPiece = (-1, -1)

    override this.Update (track, cars) =
        let myCar = cars.Data |> Array.find (fun car -> car.Id.Name = this.Name)
        let piece = track.Data.Race.Track.Pieces.[myCar.PiecePosition.PieceIndex]   

        let haveSwitchedThisPiece = switchPiece = (myCar.PiecePosition.Lap, myCar.PiecePosition.PieceIndex)

        if piece.Switch.IsSome && piece.Switch.Value && switchsRemaining > 0 && not haveSwitchedThisPiece then
            switchPiece <- (myCar.PiecePosition.Lap, myCar.PiecePosition.PieceIndex)
            switchsRemaining <- switchsRemaining - 1
            this.SwitchRight <- true

        base.Update (track, cars)
