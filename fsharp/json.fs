﻿[<AutoOpen>]
module json

open System
open System.IO
open System.Net.Sockets
open FSharp.Data

type Json = JsonProvider<"./sample.json", SampleIsList=true, RootName="message">
type JsonGameInit = JsonProvider<"./sample.gameinit.json", SampleIsList=true, RootName="message">
type JsonCarPositions = JsonProvider<"./sample.carpositions.json", SampleIsList=true, RootName="message">

let send (msg : Json.Message) (writer : StreamWriter) =
    //msg |> printfn "%A"
    writer.WriteLine (msg.JsonValue.ToString(JsonSaveOptions.DisableFormatting))
    writer.Flush()

let join (name : string) (key : string) (track : string) (color : string) =
    Json.Message(msgType = "join", data = Json.DecimalOrStringOrData(Json.Data(name = Some(name), key = Some(key), color = Some(color), botId = None, carCount = None, password = None, trackName = Some track, car = None, lapTime = None, raceTime = None, ranking = None, turboDurationTicks = None)), gameId = None, gameTick = None)

let joinRace name key track color =
    Json.Message(
        msgType = "joinRace",
            data = Json.DecimalOrStringOrData(
                Json.Data(
                    botId = Some(Json.BotId(name, key)),
                    trackName = Some(track),
                    password = Some("danny-private-match!"),
                    carCount = Some(1),
                    name = None,
                    key = None,
                    color = None,
                    car = None,
                    lapTime = None,
                    raceTime = None,
                    ranking = None,
                    turboDurationTicks = None
                )
            ),
            gameId = None,
            gameTick = None
        )

let throttle (value : float) (gameTick : int option) =
    Json.Message(msgType = "throttle", data = Json.DecimalOrStringOrData(decimal value), gameId = None, gameTick = gameTick)

let turbo =
    Json.Message(msgType = "turbo", data = Json.DecimalOrStringOrData("Zoooooooooom!"), gameId = None, gameTick = None)

let switchLeft =
    Json.Message(msgType = "switchLane", data = Json.DecimalOrStringOrData("Left"), gameId = None, gameTick = None)

let switchRight =
    Json.Message(msgType = "switchLane", data = Json.DecimalOrStringOrData("Right"), gameId = None, gameTick = None)

let ping (gameTick : int option) =
    Json.Message(msgType = "ping", data = null, gameId = None, gameTick = gameTick)

type Move =
    | Ping
    | Throttle of float
    | Turbo
    | SwitchLeft
    | SwitchRight

type LanePreference =
    | Inside
    | Outside
